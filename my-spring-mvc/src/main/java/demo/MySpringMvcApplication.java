package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

import demo.model.Product;
import demo.model.Converter;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.oxm.castor.CastorMarshaller;

@SpringBootApplication
public class MySpringMvcApplication {

    public static void main(String[] args) throws IOException {
        ApplicationContext context = SpringApplication.run(MySpringMvcApplication.class, args);
        Converter converter = new Converter();
        converter.setUnmarshaller(new CastorMarshaller());
        Product product = (Product)converter.fromXMLToObject("static/product.xml");
        System.out.println(product);

    }
}
