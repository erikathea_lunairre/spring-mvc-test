package demo.model;

import java.util.List;

public class ProductGallery {
    private List<ProductPicture> productPictures;

    public List<ProductPicture> getProductPictures() {
        return productPictures;
    }

    public void setProductPictures(List<ProductPicture> productPictures) {
        this.productPictures = productPictures;
    }
}