package demo.model;
import java.util.List;

public class Product {
    private Long id;
    private String code;
    private String name;
    private String title;
    private String quality;
    private String releaseDate;

    private String thumbPic;
    private String thumbPicSize;

    private String highPic;
    private String highPicHeight;
    private String highPicSize;
    private String highPicWidth;

    private String lowPic;
    private String lowPicHeight;
    private String lowPicSize;
    private String lowPicWidth;

    private Category category;
    private List<CategoryFeatureGroup> categoryFeatureGroups;

    private EANCode eancode;
    private List<ReasonToBuy> reasonsToBuy;
    private List<Disclaimer> disclaimers;

    private ProductFamily productFamily;
    private List<ProductFeature> productFeatures;
    private ProductGallery productGallery;

    private List<SEO> SEOs;
    private SummaryDescription summaryDescription;

    private Supplier supplier;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getThumbPic() {
        return thumbPic;
    }

    public void setThumbPic(String thumbPic) {
        this.thumbPic = thumbPic;
    }

    public String getThumbPicSize() {
        return thumbPicSize;
    }

    public void setThumbPicSize(String thumbPicSize) {
        this.thumbPicSize = thumbPicSize;
    }

    public String getHighPic() {
        return highPic;
    }

    public void setHighPic(String highPic) {
        this.highPic = highPic;
    }

    public String getHighPicHeight() {
        return highPicHeight;
    }

    public void setHighPicHeight(String highPicHeight) {
        this.highPicHeight = highPicHeight;
    }

    public String getHighPicSize() {
        return highPicSize;
    }

    public void setHighPicSize(String highPicSize) {
        this.highPicSize = highPicSize;
    }

    public String getHighPicWidth() {
        return highPicWidth;
    }

    public void setHighPicWidth(String highPicWidth) {
        this.highPicWidth = highPicWidth;
    }

    public String getLowPic() {
        return lowPic;
    }

    public void setLowPic(String lowPic) {
        this.lowPic = lowPic;
    }

    public String getLowPicHeight() {
        return lowPicHeight;
    }

    public void setLowPicHeight(String lowPicHeight) {
        this.lowPicHeight = lowPicHeight;
    }

    public String getLowPicSize() {
        return lowPicSize;
    }

    public void setLowPicSize(String lowPicSize) {
        this.lowPicSize = lowPicSize;
    }

    public String getLowPicWidth() {
        return lowPicWidth;
    }

    public void setLowPicWidth(String lowPicWidth) {
        this.lowPicWidth = lowPicWidth;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<CategoryFeatureGroup> getCategoryFeatureGroups() {
        return categoryFeatureGroups;
    }

    public void setCategoryFeatureGroups(List<CategoryFeatureGroup> categoryFeatureGroups) {
        this.categoryFeatureGroups = categoryFeatureGroups;
    }

    public EANCode getEancode() {
        return eancode;
    }

    public void setEancode(EANCode eancode) {
        this.eancode = eancode;
    }

    public List<ReasonToBuy> getReasonsToBuy() {
        return reasonsToBuy;
    }

    public void setReasonsToBuy(List<ReasonToBuy> reasonsToBuy) {
        this.reasonsToBuy = reasonsToBuy;
    }

    public List<Disclaimer> getDisclaimers() {
        return disclaimers;
    }

    public void setDisclaimers(List<Disclaimer> disclaimers) {
        this.disclaimers = disclaimers;
    }

    public ProductFamily getProductFamily() {
        return productFamily;
    }

    public void setProductFamily(ProductFamily productFamily) {
        this.productFamily = productFamily;
    }

    public List<ProductFeature> getProductFeatures() {
        return productFeatures;
    }

    public void setProductFeatures(List<ProductFeature> productFeatures) {
        this.productFeatures = productFeatures;
    }

    public ProductGallery getProductGallery() {
        return productGallery;
    }

    public void setProductGallery(ProductGallery productGallery) {
        this.productGallery = productGallery;
    }

    public List<SEO> getSEOs() {
        return SEOs;
    }

    public void setSEOs(List<SEO> SEOs) {
        this.SEOs = SEOs;
    }

    public SummaryDescription getSummaryDescription() {
        return summaryDescription;
    }

    public void setSummaryDescription(SummaryDescription summaryDescription) {
        this.summaryDescription = summaryDescription;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }
}
