package demo.model;

public class ProductFeature {
    private Long id;
    private String localized;
    private String value;
    private String no;
    private String presentationValue;
    private Boolean translated;
    private Boolean mandatory;
    private Boolean searchable;

    private CategoryFeature categoryFeature;
    private CategoryFeatureGroup categoryFeatureGroup;

    private Local local;
    private Feature feature;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocalized() {
        return localized;
    }

    public void setLocalized(String localized) {
        this.localized = localized;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getPresentationValue() {
        return presentationValue;
    }

    public void setPresentation_value(String presentationValue) {
        this.presentationValue = presentationValue;
    }

    public Boolean getTranslated() {
        return translated;
    }

    public void setTranslated(Boolean translated) {
        this.translated = translated;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Boolean getSearchable() {
        return searchable;
    }

    public void setSearchable(Boolean searchable) {
        this.searchable = searchable;
    }

    public CategoryFeature getCategoryFeature() {
        return categoryFeature;
    }

    public void setCategoryFeature(CategoryFeature categoryFeature) {
        this.categoryFeature = categoryFeature;
    }

    public CategoryFeatureGroup getCategoryFeatureGroup() {
        return categoryFeatureGroup;
    }

    public void setCategoryFeatureGroup(CategoryFeatureGroup categoryFeatureGroup) {
        this.categoryFeatureGroup = categoryFeatureGroup;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }
}