package demo.model;

public class Description {
    private Long langid;
    private String value;

    public Description() {

    }

    public Long getLangid() {
        return langid;
    }

    public void setLangid(Long langid) {
        this.langid = langid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}