package demo.model;

public class ProductPicture {
    private Long id;
    private Long no;
    private Name name;

    private String pic;
    private String picHeight;
    private String picWidth;
    private String picSize;

    private String lowPic;
    private String lowHeight;
    private String lowPicWidth;
    private String lowSize;

    private String thumbPic;
    private String thumbPicWidth;
    private String thumbPicHeight;
    private String thumbSize;

    private String original;
    private String originalHeight;
    private String originalWidth;
    private String originalSize;

    private String pic500x500;
    private String pic500x500Height;
    private String pic500x500Width;
    private String pic500x500Size;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNo() {
        return no;
    }

    public void setNo(Long no) {
        this.no = no;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPicHeight() {
        return picHeight;
    }

    public void setPicHeight(String picHeight) {
        this.picHeight = picHeight;
    }

    public String getPicWidth() {
        return picWidth;
    }

    public void setPicWidth(String picWidth) {
        this.picWidth = picWidth;
    }

    public String getPicSize() {
        return picSize;
    }

    public void setPicSize(String picSize) {
        this.picSize = picSize;
    }

    public String getLowPic() {
        return lowPic;
    }

    public void setLowPic(String lowPic) {
        this.lowPic = lowPic;
    }

    public String getLowHeight() {
        return lowHeight;
    }

    public void setLowHeight(String lowHeight) {
        this.lowHeight = lowHeight;
    }

    public String getLowPicWidth() {
        return lowPicWidth;
    }

    public void setLowPicWidth(String lowPicWidth) {
        this.lowPicWidth = lowPicWidth;
    }

    public String getLowSize() {
        return lowSize;
    }

    public void setLowSize(String lowSize) {
        this.lowSize = lowSize;
    }

    public String getThumbPic() {
        return thumbPic;
    }

    public void setThumbPic(String thumbPic) {
        this.thumbPic = thumbPic;
    }

    public String getThumbPicWidth() {
        return thumbPicWidth;
    }

    public void setThumbPicWidth(String thumbPicWidth) {
        this.thumbPicWidth = thumbPicWidth;
    }

    public String getThumbPicHeight() {
        return thumbPicHeight;
    }

    public void setThumbPicHeight(String thumbPicHeight) {
        this.thumbPicHeight = thumbPicHeight;
    }

    public String getThumbSize() {
        return thumbSize;
    }

    public void setThumbSize(String thumbSize) {
        this.thumbSize = thumbSize;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getOriginalHeight() {
        return originalHeight;
    }

    public void setOriginalHeight(String originalHeight) {
        this.originalHeight = originalHeight;
    }

    public String getOriginalWidth() {
        return originalWidth;
    }

    public void setOriginalWidth(String originalWidth) {
        this.originalWidth = originalWidth;
    }

    public String getOriginalSize() {
        return originalSize;
    }

    public void setOriginalSize(String originalSize) {
        this.originalSize = originalSize;
    }

    public String getPic500x500() {
        return pic500x500;
    }

    public void setPic500x500(String pic500x500) {
        this.pic500x500 = pic500x500;
    }

    public String getPic500x500Height() {
        return pic500x500Height;
    }

    public void setPic500x500Height(String pic500x500Height) {
        this.pic500x500Height = pic500x500Height;
    }

    public String getPic500x500Width() {
        return pic500x500Width;
    }

    public void setPic500x500Width(String pic500x500Width) {
        this.pic500x500Width = pic500x500Width;
    }

    public String getPic500x500Size() {
        return pic500x500Size;
    }

    public void setPic500x500Size(String pic500x500Size) {
        this.pic500x500Size = pic500x500Size;
    }
}