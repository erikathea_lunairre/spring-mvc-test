package demo.model;

public class SEO {
    private Long id;
    private String description;
    private String keywords;
    private String title;
    private Long langid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeywords() {
        return description;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getLangid() {
        return langid;
    }

    public void setLangid(Long langid) {
        this.langid = langid;
    }
}
