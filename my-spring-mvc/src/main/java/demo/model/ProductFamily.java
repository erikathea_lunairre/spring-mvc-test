package demo.model;

public class ProductFamily {
    private Long id;
    private String lowPic;
    private String thumbPic;
    private Name name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLowPic() {
        return lowPic;
    }

    public void setLowPic(String lowPic) {
        this.lowPic = lowPic;
    }

    public String getThumbPic() {
        return thumbPic;
    }

    public void setThumbPic(String thumbPic) {
        this.thumbPic = thumbPic;
    }
    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }
}