package demo.model;

import java.util.List;
import demo.model.Sign;

public class Measure {
    private Long id;
    private List<Sign> signs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Sign> getSigns() {
        return signs;
    }

    public void setSigns(List<Sign> signs) {
        this.signs = signs;
    }
}