package demo.model;

import java.io.InputStream;
import java.io.IOException;
import javax.xml.transform.stream.StreamSource;
import org.springframework.oxm.Unmarshaller;
import org.springframework.core.io.ClassPathResource;

public class Converter {

  private Unmarshaller unmarshaller;

  public Unmarshaller getUnmarshaller() {
    return unmarshaller;
  }

  public void setUnmarshaller(Unmarshaller unmarshaller) {
    this.unmarshaller = unmarshaller;
  }

  public Object fromXMLToObject(String xmlfile) throws IOException {

    InputStream is = null;
    try {
      ClassPathResource classPathResource = new ClassPathResource(xmlfile);
      is = classPathResource.getInputStream();
      return this.unmarshaller.unmarshal(new StreamSource(is));
    } finally {
      if (is != null) {
        is.close();
      }
    }
  }
}
