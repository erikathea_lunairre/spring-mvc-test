package demo.model;

public class SummaryDescription {
    private Long id;
    private ShortSummaryDescription shortSummaryDescription;
    private LongSummaryDescription longSummaryDescription;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShortSummaryDescription getShortSummaryDescription() {
        return shortSummaryDescription;
    }

    public void setShortSummaryDescription(ShortSummaryDescription shortSummaryDescription) {
        this.shortSummaryDescription = shortSummaryDescription;
    }

    public LongSummaryDescription getLongSummaryDescription() {
        return longSummaryDescription;
    }

    public void setLongSummaryDescription(LongSummaryDescription longSummaryDescription) {
        this.longSummaryDescription = longSummaryDescription;
    }
}